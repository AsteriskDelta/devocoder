#include "iDVC.h"
#include "Stream.impl.h"
#include "StreamHistory.impl.h"

namespace arke {
    class Stream<pcm_t>;
    class StreamHistory<pcm_t>;
};
