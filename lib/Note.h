#ifndef ARKE_AUDIO_NOTE_H
#define ARKE_AUDIO_NOTE_H
#include "iDVC.h"

namespace arke {
    class Note {
    public:
        Note();

        uint8_t tone;
        uint8_t velocity;
        struct {
            uint8_t channel : 4;
        };

        inline Note& setChannel(uint8_t c) {
            channel = c;
            return *this;
        }
        inline Note& setVelocity(uint8_t vel) {
            velocity = vel;
            return *this;
        }

        static Note FromFrequency(nvxi_t freq);
    protected:

    };
}

#endif
