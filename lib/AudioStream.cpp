#include "AudioStream.h"
#include "Audio.h"
namespace arke {
    AudioStream::AudioStream() : Stream<pcm_t>() {

    }
    AudioStream::~AudioStream() {

    }

    void AudioStream::initialize(AudioStream::Direction dir, unsigned int hwID) {

    }

    void AudioStream::process(unsigned int count, void *data) {

    }

    void AudioStream::terminate(void *data) {

    }
}
