#ifndef ARKE_MIDISTREAM_H
#define ARKE_MIDISTREAM_H
#include "iDVC.h"
#include "Stream.h"
#include "Note.h"

namespace arke {
    class MidiStream : public Stream<Note> {
    public:
        MidiStream();
        virtual ~MidiStream();

    protected:

    };
}

#endif
