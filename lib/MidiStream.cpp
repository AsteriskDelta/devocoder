#ifndef ARKE_AUDIOSTREAM_H
#define ARKE_AUDIOSTREAM_H
#include "iDVC.h"
#include "Stream.h"

namespace arke {
    class AudioStream : public Stream<pcm_t> {
    public:
        AudioStream();
        virtual ~AudioStream();

    protected:

    };
}

#endif
