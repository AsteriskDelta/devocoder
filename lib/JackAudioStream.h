#ifndef ARKE_JACK_AUDIOSTREAM_H
#define ARKE_JACK_AUDIOSTREAM_H
#include "iDVC.h"
#include "AudioStream.h"

struct _jack_port;
typedef _jack_port jack_port_t;

namespace arke {
    class JackAudioStream : public AudioStream {
    public:
        JackAudioStream();
        virtual ~JackAudioStream();

        virtual void initialize(Direction dir, unsigned int hwID) override;
        virtual void process(unsigned int count, void *data) override;
        virtual void terminate(void *data) override;

        jack_port_t *handle;
    protected:

    };
}

#endif
