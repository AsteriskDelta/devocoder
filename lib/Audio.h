#ifndef ARKE_AUDIO_H
#define ARKE_AUDIO_H
#include "iDVC.h"
#include <vector>

namespace arke {
    class AudioStream;

    class Audio {
    public:
        Audio();
        virtual ~Audio();

        inline bool running() const { return isRunning; };

        static void TerminateAll(void* data);
        static int Process(unsigned int id, void* data);

        std::vector<AudioStream*> streams;
        virtual void* handle() const;

        virtual AudioStream* addInputStream();
        virtual AudioStream* addOutputStream();
    protected:
        virtual bool initialize();
        virtual void terminate(void *data);
        virtual bool activate();

        virtual void process(unsigned int id, void *data);


        bool isRunning;

        static std::vector<Audio*> Instances;
    };
}

#endif
