#include "JackAudioStream.h"
#include "Audio.h"
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <jack/jack.h>

namespace arke {
    JackAudioStream::JackAudioStream() : AudioStream() {

    }
    JackAudioStream::~JackAudioStream() {

    }

    void JackAudioStream::initialize(JackAudioStream::Direction dir, unsigned int hwID) {
        static unsigned int regID = 0;
        handle = jack_port_register (reinterpret_cast<jack_client_t*>(this->parent()->handle()), std::to_string(regID).c_str(),
        					 JACK_DEFAULT_AUDIO_TYPE,
        					 this->direction == Direction::Input?JackPortIsInput : JackPortIsOutput, 0);
        regID++;
    }

    void JackAudioStream::process(unsigned int count, void *data) {
        jack_default_audio_sample_t *buffer;
        buffer = reinterpret_cast<jack_default_audio_sample_t*>(jack_port_get_buffer(handle, count));
        lastFrame.resize(count);
        if(this->direction == AudioStream::Input) memcpy(buffer, lastFrame.data(), count * sizeof(jack_default_audio_sample_t));
        else memcpy(lastFrame.data(), buffer, count * sizeof (jack_default_audio_sample_t));
    }

    void JackAudioStream::terminate(void *data) {

    }
}
