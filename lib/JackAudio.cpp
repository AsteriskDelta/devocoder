#include "JackAudio.h"
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <jack/jack.h>
#include <ARKE/Application.h>
#include "AudioStream.h"

namespace arke {
    JackAudio::JackAudio() : client(nullptr) {

    }
    JackAudio::~JackAudio() {

    }

    bool JackAudio::initialize() {
        const char **ports;
    	this->name = Application::GetName();
    	const char *server_name = NULL;
    	jack_options_t options = JackNullOption;
    	jack_status_t status;

    	/* open a client connection to the JACK server */

    	client = jack_client_open (name.c_str(), options, &status, server_name);
    	if (client == NULL) {
    		fprintf (stderr, "jack_client_open() failed, "
    			 "status = 0x%2.0x\n", status);
    		if (status & JackServerFailed) {
    			fprintf (stderr, "Unable to connect to JACK server\n");
    		}
    		exit (1);
    	}
    	if (status & JackServerStarted) {
    		fprintf (stderr, "JACK server started\n");
    	}
    	if (status & JackNameNotUnique) {
    		name = jack_get_client_name(client);
    		fprintf (stderr, "unique name `%s' assigned\n", name.c_str());
    	}

    	jack_set_process_callback (client, Audio::Process, 0);

    	jack_on_shutdown (client, Audio::TerminateAll, 0);

    	printf ("engine sample rate: %" PRIu32 "\n",jack_get_sample_rate (client));
        return true;
    }
    void JackAudio::terminate(void *data) {
        if(client != nullptr) jack_client_close(client);
    }

    bool JackAudio::activate() {
        if (jack_activate (client)) {
    		fprintf (stderr, "cannot activate client");
    		return false;
    	}
    }

    void* JackAudio::handle() const {
        return reinterpret_cast<void*>(client);
    }

    void JackAudio::process(unsigned int count, void *data) {
        for(AudioStream *stream : this->streams) stream->process(count, data);
    }

    AudioStream* JackAudio::addInputStream() {
        AudioStream *ret =  new JackAudioStream();
        ret->handle = jack_port_register (client, "input",
					 JACK_DEFAULT_AUDIO_TYPE,
					 JackPortIsInput, 0);
        return ret;
    }
    AudioStream* JackAudio::addOutputStream() {
        AudioStream *ret =  new JackAudioStream();
        ret->handle = jack_port_register (client, "output",
					 JACK_DEFAULT_AUDIO_TYPE,
					 JackPortIsOutput, 0);
        return ret;
    }
}
