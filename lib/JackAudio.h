#ifndef ARKE_AUDIO_JACK_H
#define ARKE_AUDIO_JACK_H
#include "iDVC.h"
#include "Audio.h"

struct _jack_port;
struct _jack_client;
typedef _jack_port jack_port_t;
typedef _jack_client jack_client_t;

namespace arke {

    class AudioStream;

    class JackAudio : public Audio {
    public:
        JackAudio();
        virtual ~JackAudio();

        jack_client_t *client;
        virtual void* handle() const;

        virtual AudioStream* addInputStream() override;
        virtual AudioStream* addOutputStream() override;
    protected:
        std::string name;

        virtual bool initialize() override;
        virtual void terminate(void *data) override;
        virtual void process(unsigned int count, void *data) override;
        virtual bool activate() override;

    };
}

#endif
