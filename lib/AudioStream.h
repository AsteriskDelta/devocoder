#ifndef ARKE_AUDIOSTREAM_H
#define ARKE_AUDIOSTREAM_H
#include "iDVC.h"
#include "Stream.h"
#include <rfftw.h>

namespace arke {
    class Audio;

    class AudioStream : public Stream<pcm_t> {
    public:
        AudioStream();
        virtual ~AudioStream();

        enum Direction : uint8_t {
            Input, Output, Invalid
        };
        Direction direction;

        virtual void initialize(Direction dir, unsigned int hwID);
        virtual void process(unsigned int count, void *data);
        virtual void terminate(void *data);

        std::vector<pcm_t> lastFrame;

        std::vector<pcm_t> spectralSource;
        std::vector<pcm_t> spectralOutput;
        std::vector<pcm_t> spectrum;

        Audio *parentPtr;
        inline Audio *parent() const {
            return parentPtr;
        }
    protected:
    };
}

#endif
