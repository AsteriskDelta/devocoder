#include "Audio.h"

namespace arke {
    std::vector<Audio*> Audio::Instances;

    Audio::Audio() {

    }
    Audio::~Audio() {
        if(this->running()) this->terminate(nullptr);
    }

    bool Audio::initialize() {
        return false;
    }
    void Audio::terminate(void *data) {
        _unused(data);
    }
    bool Audio::activate() {
        return false;
    }

    void* Audio::handle() const {
        return nullptr;
    }

    void Audio::process(unsigned int id, void *data) {
        _unused(id,data);
    }

    void Audio::TerminateAll(void *data) {
        for(Audio* instance : Instances) {
            instance->terminate(data);
        }
    }
    int Audio::Process(unsigned int id, void* data) {
        for(Audio* instance : Instances) {
            instance->process(id, data);
        }
    }

    AudioStream* Audio::addInputStream() {
        return nullptr;
    }
    AudioStream* Audio::addOutputStream() {
        return nullptr;
    }
}
