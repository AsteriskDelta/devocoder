#include "Stream.h"

namespace arke {

    template<typename T>
    Stream<T>::Stream() {

    }
    template<typename T>
    Stream<T>::~Stream() {

    }

    template<typename T>
    void Stream<T>::addEntry(const Stream<T>::Entry& entry) {
        StreamHistory<T>::addEntry(entry);
    }
};
