#ifndef DVC_INDEX_HIST_H
#define DVC_INDEX_HIST_H
#include "iDVC.h"
#include <vector>

namespace arke {
    template<typename T>
    class StreamHistory {
    public:
        typedef T StreamEntry;
        /* Chosen by single-core benchmark: (time, in s)
          16:  13.819
          32:  4.483
          64:  3.808
          128: 3.617    <-- optimal minimization
          256: 4.100
        */
        static constexpr unsigned int MaxEntries = 128;

        Time min, max;
        StreamEntry average;

        StreamHistory();
        virtual ~StreamHistory();

        StreamEntry sample(Time t, Time range);
        StreamEntry sample(Time t, Time radius, unsigned short taps, Num gaborFrequency = M_PI, int forceSign = 0);
        virtual void addEntry(const StreamEntry& entry);//Overridden to allow for HDI assimilation

        StreamHistory* expand();

        bool full() const;
        bool contains(const Time& time) const;

        void update();

        StreamHistory* root();
    protected:
        StreamHistory *parent;
        bool leaf;
        bool dirty;

        std::vector<StreamHistory*> branches;
        std::vector<StreamEntry> entries;
    };

};

#endif
