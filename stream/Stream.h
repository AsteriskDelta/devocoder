#ifndef DVC_INDEX_H
#define DVC_INDEX_H
#include "iDVC.h"
#include "StreamHistory.h"

namespace arke {
    template<typename T>
    class Stream : public StreamHistory<T> {
    public:
        typedef T Entry;
        Stream();
        virtual ~Stream();

        virtual void addEntry(const Entry& entry) override;
    };
};

#endif
