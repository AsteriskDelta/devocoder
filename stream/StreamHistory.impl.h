#include "StreamHistory.h"
//#include "stream/Distribution.h"
#include <NVX/NGaussian.h>

namespace arke {
    template<typename T>
    StreamHistory<T>::StreamHistory() : min(0), max(0), average(), parent(nullptr), leaf(true), dirty(true), entries(nullptr) {

    }
    template<typename T>
    StreamHistory<T>::~StreamHistory() {
        if(entries != nullptr) delete entries;
        for(StreamHistory<T> *const branch : branches) delete branch;
    }
    template<typename T>
    bool StreamHistory<T>::contains(const Time& time) const {
        return this->min <= time && this->max > time;
        /*
        if(this->leaf) {
            if(entries == nullptr || entries->size() < 2) return false;
            return entries->begin()->time >= time && entries->end()->time <= time;
        } else {
            for(StreamHistory<T> *const branch : branches) {
                if(branch->contains(time)) return true;
            }

            return false;
        }*/
    }
    template<typename T>
    T StreamHistory<T>::sample(Time t, Time range) {
        if(this->leaf) {
            if(entries == nullptr) return StreamEntry();
            StreamEntry *bestEntry = nullptr; Time bestTime = std::numeric_limits<Time>::max();
            //std::cout << "\tsample over " << entries->size() << ": ";
            for(StreamEntry &entry : *entries) {
                Time timeErr = fabs(entry.time - t);
                if(timeErr < bestTime) {
                    bestEntry = &entry;
                    bestTime = timeErr;
                }
            }
            std::cout << "best = " << bestEntry << "(" << bestEntry->str() << ") @ " << bestTime << " for t=" << t << " over " << range << "\n";

            if(bestEntry == nullptr) return StreamEntry();
            else return *bestEntry;
        } else {
            for(StreamHistory<T> *const branch : branches) {
                if(branch->contains(t)) return branch->sample(t, range);
            }

            if(branches.size() == 0) return StreamEntry();
            else return branches.back()->sample(t, range);
        }
    }

    template<typename T>
    T StreamHistory<T>::sample(Time t, Time radius, unsigned short taps, Num gaborFrequency, int forceSign) {
        Gaussian<T> gabor;//QuasiGabor<double> gabor;
        //gabor.setCenter(t);
        //gabor.setRadius(radius);
        gabor.set(t, radius);
        //gabor.frequency = gaborFrequency;

        //Time totalSpan = (forceSign == 0)? 2 * radius : radius;
        //Time smpBase = (forceSign <= 0)? (t - radius) : radius;

        StreamEntry ret;
        Num denom = 0.0;

        //Ensure we always take 'tap' taps
        const int tapAbs = forceSign == 0? taps/2 : taps;
        const int tapMin = forceSign == 1? 0 : -tapAbs;
        const int tapMax = forceSign == -1? 0 : tapAbs;
        Num totalWeight = 0.0;

        Time avgTime = 0.0;
        for(int i = tapMin; i <= tapMax; i++) {
            double smpWeight = i / double(taps);
            Time smpTime = t + Time(i)/Time(tapAbs) * radius;//gabor.invweight(smpWeight);
            Time smpRadius = radius/Time(tapAbs);//gabor.span(smpTime);

            Num smpCoeff = gabor(smpTime);
            const StreamEntry tapEntry = this->sample(smpTime, smpRadius);
            ret = ret + tapEntry * smpCoeff;
            avgTime += tapEntry.time * smpCoeff;
            totalWeight += smpCoeff;
            std::cout << "\t\t" <<std::setprecision(20) <<smpTime << std::setprecision(6) << "@"<<totalWeight<<" * " << smpCoeff << "\t";
            //std::cout << "\t\t sampled at " << std::setprecision(20) <<smpTime << std::setprecision(6) <<" for " << this->sample(smpTime, smpRadius).str() << ", ret = " << ret.str() << "\n";
            denom += smpCoeff;
        }
        //return ret;
        if(denom == 0.0) return StreamEntry();
        else {
            ret.time = avgTime / denom;
            return ret / denom;
        }

        //Linear sampling, no no
        /*for(int i = 0; i < taps; i++) {
            Time smp = i / taps * totalSpan + smpBase;
            //std::cout << gabor.invweight(smp) << ", " << gabor(gabor.invweight(smp)) << ", " << gabor.weight(gabor.invweight(smp)) << ", " << gabor.span(gabor.invweight(smp)) << "\n";
            Time tapRadius = gabor.span(smp);
        }*/
    }
    template<typename T>
    void StreamHistory<T>::addEntry(const T& entry) {
        //Expand upwards if needed
        if(parent == nullptr && this->full()) {
            /*this->parent = new StreamHistory<T>();
            parent->leaf = false;
            parent->branches.push_back(this);
            parent->update();*/
            this->expand();
            return this->addEntry(entry);
        } else if(this->full()) return parent->addEntry(entry);

        this->dirty = true;

        if(!this->leaf) {//Delegate
            for(StreamHistory<T> *const branch : branches) {
                if(!branch->full() && branch->contains(entry.time)) {
                    return branch->addEntry(entry);
                }
            }

            if(!(*branches.rbegin())->full()) return (*branches.rbegin())->addEntry(entry);

            //Only is reached when not full (see above loop invocation), so this is known to be valid
            StreamHistory<T> *newNode = this->expand();
            newNode->addEntry(entry);
        } else {
            if(entries == nullptr) {
              entries = new std::vector<StreamEntry>();
              entries->reserve(MaxEntries);
            };
            entries->push_back(entry);
            //std::cout << "entries size: " << entries->size() << "\n";
            //usleep(100000);
        }
    }
    template<typename T>
    StreamHistory<T>* StreamHistory<T>::expand() {
        StreamHistory<T> *ret = new StreamHistory<T>();
        ret->parent = this;

        if(this->full()) {
          if(this->leaf) {//Promote to branch
            ret->entries = this->entries;
            this->entries = nullptr;
            this->leaf = false;
            ret->leaf = true;
          } else {
            ret->leaf = false;
            ret->branches = this->branches;
            this->branches.clear();
          }
        }

        //ret->update();

        branches.push_back(ret);
        this->update();
        //std::cout << "Expanded index to have " << branches.size() << " branches\n";
        return ret;
    }
    template<typename T>
    bool StreamHistory<T>::full() const {
        return (!leaf && branches.size() >= MaxEntries) || (leaf && entries != nullptr && entries->size() >= MaxEntries);
    }
    template<typename T>
    void StreamHistory<T>::update() {
        if(!this->dirty) return;
        if(!leaf) for(StreamHistory<T> *const branch : branches) branch->update();

        this->dirty = false;
        average.clear();
        Time spanWeight = 0;
        min = std::numeric_limits<Time>::max();
        max = 0;

        if(leaf) {
            if(entries == nullptr) {
              average = StreamEntry();
              return;
            }

            for(unsigned int i = 0; i < entries->size(); i++) {
                const StreamEntry& entry = (*entries)[i];
                if(entry.time < min) min = entry.time;
                if(entry.time > max) max = entry.time;

                Time tDelta = 1;
                if(entries->size() > 1) {
                    if(i == 0) tDelta = ((*entries)[i+1].time - entry.time);
                    else if(i == entries->size() - 1) tDelta = entry.time - (*entries)[i-1].time;
                    else tDelta = ((*entries)[i+1].time - (*entries)[i-1].time) / 2;
                }

                spanWeight += tDelta;
                average += entry * tDelta;
            }

            average /= spanWeight;
        } else {
            for(StreamHistory<T> *const branch : branches) {
                min = std::min(min, branch->min);
                max = std::max(max, branch->max);

                Time tDelta = branch->max - branch->min;
                average += branch->average * tDelta;
                spanWeight += tDelta;
            }

            average /= spanWeight;
        }
    }

    template<typename T>
    StreamHistory<T>* StreamHistory<T>::root() {
        if(parent == nullptr) return this;
        else return parent->root();
    }
};
